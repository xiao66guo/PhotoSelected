//
//  UIButton+Extension.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit


extension UIButton {
    ///  便利构造函数
    ///
    ///  - parameter title:     title
    ///  - parameter imageName: imageName
    ///  - parameter color:     color
    ///  - parameter fontSize:  fontSize
    ///
    ///  - returns: 返回一个UIButton
    convenience init (title: String, imageName: String, color: UIColor, fontSize: CGFloat){
        // 实例化当前对象
        self.init()
        
        setTitle(title, forState: UIControlState.Normal)
        setTitleColor(color, forState: UIControlState.Normal)
        setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
    }
    
    ///  便利构造函数
    ///
    ///  - parameter imageName: imageName
    ///
    ///  - returns: UIButton
    convenience init (imageName: String){
        // 实例化当前对象
        self.init()
        
        setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        setImage(UIImage(named: imageName + "_highlighted"), forState: UIControlState.Highlighted)
        
        sizeToFit()
    }

}
